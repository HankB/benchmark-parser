# Benchmark-parser

Parse output from common benchmarks.

## Description / Motivation

I frequently benchmark my H/W so I get a handle on what is going on. This usually includes storage benchmarks (and that's where I plan to start.) Once the benchmark is run, I have been transcribing the results into Markdown documents. This is tedious and the results are not easy to compare. A better strategy is to store the results in a database that can then be queried. Manual entry would still be tedious so the desire here is to provide scripts that can parse the results and put the interesting stuff in a database that can then be queried.

## Policies

* Throughput numbers will ne expressed as MiB/s.
* Time stamps will be expressed as ISO 8601 strings `2022-07-14T1933` or `2022-07-14` if time is not available. Local time with no time zone indicator is allowed. Elapsed time will be `mm:ss`.
* For the different benchmarks various fields will be stored in the database and will necessitate different tables for each particular benchmark. Some common fields will be recorded for each benchmark and these generally capture information external to the benchmarks themselves and will include
  * hostname
  * device type (e.g. NVME, SATA, USB)
  * device brand/model, enough information to identify the H/W. (e.g. "850 EVO", "EX950", "Inland Professional")
  * filesystem (e.g. "EXT4", "VFAT", "ZFS RAIDZ1")
  * wall time, time reported by `time` command to execute the benchmark.
  * command including all options.
  * ordinal of run. 1 (first), 2 (second) and so on to indicate if caches have been warmed.

## Project status

* `parsers/parse-dd-benchmark.py` - Parses `dd-benchmark.sh` output and returns values. Now converted to a module (albeit with awkward naming) and a single unit test created. Need to address other values that should be saved with the benchmark results and add code to grab them from the command line or provide sensible defaults.

## Installation

TBD

## Usage

The intended usage is to execute the benchmark and pipe the results into the parser which will extract interesting information and insert into the database.

## Support

TBD

## Roadmap

Implement one parser as well as a database injector and then go from there.

## Contributing

I can use the help!

## Authors and acknowledgment

I will thank you for any help!

## License

MIT, no beer clause.

## Errata

Eventually the results will be stored in a database that will either be Sqlite3 on a designated machine or (more probably) MariaDB in a designated server. Some PCs may not have network access to the Sqlite file or MariaDB server and it is expected that a command executed remotely using SSH will perform the benchmark and parsing will be performed on the local machine. For that reason the local host name cannot be associated with the results.

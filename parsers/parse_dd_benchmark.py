#!/usr/bin/env python3

"""
Parse the following output from dd-benchmark.sh
====
hbarta@rocinante:~$ time -p dd-benchmark.sh
Creating test file
4096+0 records in
4096+0 records out
4294967296 bytes (4.3 GB, 4.0 GiB) copied, 11.4708 s, 374 MB/s
reading back test file
[sudo] password for hbarta: 
4096+0 records in
4096+0 records out
4294967296 bytes (4.3 GB, 4.0 GiB) copied, 0.640565 s, 6.7 GB/s
write performance
4096+0 records in
4096+0 records out
4294967296 bytes (4.3 GB, 4.0 GiB) copied, 2.58628 s, 1.7 GB/s
4096+0 records in
4096+0 records out
4294967296 bytes (4.3 GB, 4.0 GiB) copied, 2.51676 s, 1.7 GB/s
read performance
4096+0 records in
4096+0 records out
4294967296 bytes (4.3 GB, 4.0 GiB) copied, 2.80353 s, 1.5 GB/s
4096+0 records in
4096+0 records out
4294967296 bytes (4.3 GB, 4.0 GiB) copied, 2.67568 s, 1.6 GB/s
real 38.88
user 0.08
sys 21.04
hbarta@rocinante:~$
====

DD reports results in whatever scale is convenient but a constant scale is
required for sensible comparison. The choice to stora as MB/s seems like a
good first choice. From this example we would get

[creation]=384
[read_back]=7900
[write1]=131
[write2]=141
[read1]=1400
[read2]=1500
[wall_clock]

Note that the display of the extracted resuts implies a dictionary. In addition
to the values extracted fomr the output, there will be other values that need
to be stored with the results to make sense. These include

[hostname]=olive
[device]=RAIDZ1
[filesystem]=ZFS
[comments]="5x 500GB SSD"
[wall_clock]="54:32"
"""

# function to parse the input


def parse_dd_benchmark(text):
    lines = text.splitlines()
    states = ['creation',
              'read_back',
              'write1', 'write2',
              'read1', 'read2']
    state_ix = 0

    results = {}
    for line in lines:
        if line.find('copied') >= 0:
            tokens = line.split(' ')
            # calculate throughput in MiB abd round
            throughput = int(tokens[0])/float(tokens[7])
            throughput = round(throughput/(1024*1024), 2)
            results[states[state_ix]] = throughput
            # print(tokens[0], tokens[7], states[state_ix],
                  # results[states[state_ix]])
            state_ix += 1
        elif line.find('real') == 0:
            tokens = line.split(' ')
            results['wall_clock'] = tokens[1]
    return results


if __name__ == "__main__":
    input = """Creating test file
4096+0 records in
4096+0 records out
4294967296 bytes (4.3 GB, 4.0 GiB) copied, 11.4708 s, 374 MB/s
reading back test file
[sudo] password for hbarta: 
4096+0 records in
4096+0 records out
4294967296 bytes (4.3 GB, 4.0 GiB) copied, 0.640565 s, 6.7 GB/s
write performance
4096+0 records in
4096+0 records out
4294967296 bytes (4.3 GB, 4.0 GiB) copied, 2.58628 s, 1.7 GB/s
4096+0 records in
4096+0 records out
4294967296 bytes (4.3 GB, 4.0 GiB) copied, 2.51676 s, 1.7 GB/s
read performance
4096+0 records in
4096+0 records out
4294967296 bytes (4.3 GB, 4.0 GiB) copied, 2.80353 s, 1.5 GB/s
4096+0 records in
4096+0 records out
4294967296 bytes (4.3 GB, 4.0 GiB) copied, 2.67568 s, 1.6 GB/s
real 38.88
user 0.08
sys 21.04"""
    benchmark = parse_dd_benchmark(input)
    print(benchmark)

#!/usr/bin/env python3

import unittest
from parse_dd_benchmark import parse_dd_benchmark

input = """Creating test file
4096+0 records in
4096+0 records out
4294967296 bytes (4.3 GB, 4.0 GiB) copied, 11.4708 s, 374 MB/s
reading back test file
[sudo] password for hbarta: 
4096+0 records in
4096+0 records out
4294967296 bytes (4.3 GB, 4.0 GiB) copied, 0.640565 s, 6.7 GB/s
write performance
4096+0 records in
4096+0 records out
4294967296 bytes (4.3 GB, 4.0 GiB) copied, 2.58628 s, 1.7 GB/s
4096+0 records in
4096+0 records out
4294967296 bytes (4.3 GB, 4.0 GiB) copied, 2.51676 s, 1.7 GB/s
read performance
4096+0 records in
4096+0 records out
4294967296 bytes (4.3 GB, 4.0 GiB) copied, 2.80353 s, 1.5 GB/s
4096+0 records in
4096+0 records out
4294967296 bytes (4.3 GB, 4.0 GiB) copied, 2.67568 s, 1.6 GB/s
real 38.88
user 0.08
sys 21.04"""

class TestParsing(unittest.TestCase):

    def test_parse_input(self):
        benchmark = parse_dd_benchmark(input)
        self.assertEqual(benchmark,
                         {'creation': 357.08, 'read_back': 6394.35,
                          'write1': 1583.74, 'write2': 1627.49,
                          'read1': 1461.02, 'read2': 1530.83,
                          'wall_clock': '38.88'})


if __name__ == '__main__':
    unittest.main()